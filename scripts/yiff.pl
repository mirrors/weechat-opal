# original yiff.pl by jakk, berry, and mike
# this version is for weechat and supports any db file

use utf8;
use strict;
use warnings;
no strict 'subs';

my $SCRIPT_NAME = 'yiff';
my $SCRIPT_AUTHOR = 'The Krusty Krab <wowaname@volatile.ch>';
my $SCRIPT_VERSION = '1.0';
my $SCRIPT_LICENCE = 'WTFPL';
my $SCRIPT_DESC = 'Rewrite of irssi/X-Chat yiff.pl for WeeChat';

if (weechat::register($SCRIPT_NAME, $SCRIPT_AUTHOR, $SCRIPT_VERSION,
 $SCRIPT_LICENCE, $SCRIPT_DESC, '', '')) {
	weechat::hook_command('yiff', 'yiffs a selected person', '[-dbfile] [nick]',
		"dbfile: use a different database file than the default ".
		"(include hyphen before name)\n".
		"nick: person to yiff", '', 'cmd_yiff', '');
	weechat::hook_command('ryiff', 'yiffs a random person', '[-dbfile]',
		"dbfile: use a different database file than the default ".
		"(include hyphen before name)", '', 'cmd_ryiff', '');

	my %OPTIONS = (
		dir => ['Database directory',
			weechat::info_get('weechat_dir', '').'/yiffs'],
		db => ['Default database', 'yiffs'],
		solodb => ['Default database when nick is omitted', 'solos'],
		);

	for my $option (keys %OPTIONS) {
		weechat::config_set_plugin($option, $OPTIONS{$option}[1])
		 unless weechat::config_is_set_plugin($option);
		weechat::config_set_desc_plugin($option, $OPTIONS{$option}[0]);
	}
}

sub nicklist {
	my $buffer = $_[0];
	my @nicks;
	my $iptr = weechat::infolist_get('irc_nick', '',
		weechat::buffer_get_string($buffer, 'localvar_server').','.
		weechat::buffer_get_string($buffer, 'localvar_channel'));

	return () unless $iptr;

	while (weechat::infolist_next($iptr)) {
		push @nicks, weechat::infolist_string($iptr, 'name');
	}

	weechat::infolist_free($iptr);

	return @nicks;
}

sub yiff {
	my ($buffer, $dbfile, $target, @users) = @_;
	my $user = $users[rand @users];
	my $nick = weechat::buffer_get_string($buffer, 'localvar_nick');
	my $channel = weechat::buffer_get_string($buffer, 'localvar_channel');

	if ($dbfile) { $dbfile =~ s/^-// }
	elsif ($target) { $dbfile = weechat::config_get_plugin('db') }
	else { $dbfile = weechat::config_get_plugin('solodb') }

	if (open YIFFFILE, "<", weechat::config_get_plugin('dir')."/$dbfile") {
		my @yiffs = <YIFFFILE>;
		close YIFFFILE;
		my $yiff = $yiffs[rand @yiffs];
		if ($target) { $yiff =~ s/\$target/$target/g }
		$yiff =~ s/\$user/$user/g;
		$yiff =~ s/\$nick/$nick/g;
		$yiff =~ s/\$channel/$channel/g;
		weechat::command($buffer, "/me $yiff");
	} else {
		weechat::print($buffer, weechat::prefix('error').
			"Cannot open '$dbfile' ".
			"(make sure the db directory and file exists)");
		return weechat::WEECHAT_RC_ERROR;
	}

	return weechat::WEECHAT_RC_OK;
}

sub cmd_yiff {
	my $buffer = $_[1];
	my ($dbfile, $target) = $_[2] =~ /^(-[^ ]+|) *([^ ]*)/;
	my @users = nicklist($buffer);

	return yiff($buffer, $dbfile, $target, @users);
}

sub cmd_ryiff {
	my $buffer = $_[1];
	my ($dbfile) = $_[2] =~ /^(-[^ ]+|)/;
	my @users = nicklist($buffer);

	my $target = $users[rand @users];

	return yiff($buffer, $dbfile, $target, @users);
}
